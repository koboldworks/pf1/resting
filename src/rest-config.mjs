import { RestData } from './rest-data.mjs';
import { CFG } from './common.mjs';

/* global RollPF */

/**
 * Find variable names.
 *
 * @param {string} f Formula
 * @returns {string[]} Array of `@variables`
 */
const getVariables = (f) => f.match(/@[\w.]+/g)
	?.map(m => m.substring(1)) ?? [];

/**
 * Find orphaned comments.
 *
 * @param {string} f Formula
 * @returns {string[]} Array of `@variables`
 */
const getOrphans = (f) => f.match(/@[\w.]+\s+\[[^\]]*]?/g) ?? [];

/**
 *
 * @param {object} p
 * @param {string} p.formula
 * @param {object} p.rollData
 * @returns {string[]} Errors
 */
const validateFormula = async ({ formula, rollData = {} } = {}) => {
	if (formula.trim().length == 0) return [];

	const vars = getVariables(formula);
	const errors = [];

	const roll = RollPF.safeRoll(formula, rollData);
	if (roll.err) errors.push({ match: roll.err, error: 'Koboldworks.Resting.Validation.EvalFail' });

	for (const v of vars) {
		const hasPath = foundry.utils.hasProperty(rollData, v) || ['time', 'heal', 'bonusHeal', 'fullRest'].includes(v);
		if (!hasPath) errors.push({ match: `@${v}`, missing: true, error: 'Koboldworks.Resting.Validation.Missing' });
	}

	const orphans = getOrphans(formula);
	for (const orphan of orphans)
		errors.push({ match: orphan, orphan: true, error: 'Koboldworks.Resting.Validation.Orphan' });

	return errors;
};

export class RestingFormulaConfig extends FormApplication {
	healData;

	constructor(object = {}, options = {}) {
		super(object, options);
		this.healData = game.settings.get(CFG.id, 'resting');

		this.fakeActor = new Actor.implementation({ type: 'character', name: 'no-one' }); // fake actor
		this.fakeActor.reset(); // Necessary?
		this.fakeRollData = this.fakeActor.getRollData();
	}

	/** @override */
	static get defaultOptions() {
		const options = super.defaultOptions;
		return {
			...options,
			id: 'koboldworks-resting-config',
			title: game.i18n.localize('Koboldworks.Resting.Settings.Formula.Title'),
			classes: [...options.classes, 'koboldworks', 'resting-config'],
			width: 480,
			height: 'auto',
			closeOnSubmit: false,
			submitOnClose: false,
			submitOnChange: true,
		};
	}

	get template() {
		return `modules/${CFG.id}/template/rest-config.hbs`;
	}

	async _validate() {
		const healKeys = { hp: 'HitPoints', nl: 'NonLethal', abl: 'AbilityScore' };
		const rollData = this.fakeRollData;

		const hd = this.healData;

		const formulas = {
			hp: hd.hp,
			nl: hd.nl,
			abl: hd.abl,
		};

		for (const [key, i18nKey] of Object.entries(healKeys)) {
			formulas[key].label = `Koboldworks.Resting.${i18nKey}`;
			formulas[key].hint = `Koboldworks.Resting.Settings.Formula.${i18nKey}.Hint`;

			for (const field of ['formula', 'bonus', 'longTerm']) {
				const formula = hd[key][field];
				const errors = await validateFormula({ formula, rollData });
				this.healData[key][`${field}_errors`] = errors;
				if (errors.length) console.warn(formula, '\nERRORS:', errors);
			}
		}

		const errors = await validateFormula({ formula: this.healData.fullRest, rollData });
		this.healData.fullRest_errors = errors;
	}

	async getData() {
		const context = super.getData();

		await this._validate();

		context.heal = this.healData;
		context.formulas = {
			hp: context.heal.hp,
			nl: context.heal.nl,
			abl: context.heal.abl,
		};

		context.vars = {
			time: 'Koboldworks.Resting.Settings.Formula.Vars.Time',
			heal: 'Koboldworks.Resting.Settings.Formula.Vars.Heal',
			bonusHeal: 'Koboldworks.Resting.Settings.Formula.Vars.BonusHeal',
			fullRest: 'Koboldworks.Resting.Settings.Formula.Vars.FullRest',
		};

		return context;
	}

	_onResetDefaults(event) {
		event.preventDefault();

		this.healData = new RestData();

		this.render(false);
	}

	async _save(event) {
		event.preventDefault();
		await this.submit({ preventClose: true, preventRender: true });

		await this._validate();

		const hd = this.healData;
		const errors = [...hd.fullRest_errors];
		['hp', 'nl', 'abl'].forEach(key => ['formula', 'bonus', 'longTerm']
			.forEach(field => errors.push(...hd[key][`${field}_errors`])));

		if (errors.length)
			return void ui.notifications.error(game.i18n.localize('Koboldworks.Resting.Settings.Formula.SaveRejected'));

		await game.settings.set(CFG.id, 'resting', this.healData);
		console.log(game.settings.get(CFG.id, 'resting'));
		this.close();
	}

	/**
	 * @param {JQuery<HTMLElement>} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const [html] = jq;

		html.querySelector('button[name="reset"]')
			.addEventListener('click', this._onResetDefaults.bind(this));

		html.querySelector('button[name="save"]')
			.addEventListener('click', this._save.bind(this));
	}

	async _updateObject(event, form) {
		const rollData = this.fakeRollData;
		rollData.time = 33;
		rollData.bonus = 15;
		rollData.heal = 15;

		const newData = new RestData(foundry.utils.expandObject(form).heal);

		const hd = this.healData;
		const errors = [...hd.fullRest_errors];
		['hp', 'nl', 'abl'].forEach(key => ['formula', 'bonus', 'longTerm']
			.forEach(field => errors.push(...hd[key][`${field}_errors`])));

		this.healData = newData;

		this.render();
	}
}
