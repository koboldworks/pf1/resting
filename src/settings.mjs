import { RestData } from './rest-data.mjs';
import { RestingFormulaConfig } from './rest-config.mjs';
import { CFG } from './common.mjs';

Hooks.once('init', () => {
	game.settings.register(CFG.id, 'resting', {
		scope: 'world',
		type: RestData,
		default: new RestData(),
		config: false,
	});

	game.settings.registerMenu(CFG.id, 'formulaMenu', {
		id: 'koboldworks-resting-config',
		name: 'Koboldworks.Resting.Settings.Formula.Label',
		label: 'Koboldworks.Resting.Settings.Formula.Title',
		hint: 'Koboldworks.Resting.Settings.Formula.Hint',
		icon: 'fas fa-bed', // button icon
		type: RestingFormulaConfig, // FormApplication reference
		restricted: true, // GM only if true
	});

	game.settings.register(CFG.id, 'chatCard', {
		name: 'Koboldworks.Resting.Settings.ChatCard.Label',
		hint: 'Koboldworks.Resting.Settings.ChatCard.Hint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'transparency', {
		name: 'Koboldworks.Resting.Settings.ChatCard.Transparency',
		hint: 'Koboldworks.Resting.Settings.ChatCard.TransparencyHint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'deltaOnly', {
		name: 'Koboldworks.Resting.Settings.ChatCard.DeltaOnly',
		hint: 'Koboldworks.Resting.Settings.ChatCard.DeltaOnlyHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'declareUseRestore', {
		name: 'Koboldworks.Resting.Settings.ChatCard.DeclareUseRestore',
		hint: 'Koboldworks.Resting.Settings.ChatCard.DeclareUseRestoreHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'debug', {
		name: 'Debug',
		type: Boolean,
		default: false,
		scope: 'client',
		config: false,
	});
});

Hooks.once('ready', () => {
	game.modules.get(CFG.id).api = {
		debug: (enabled) => game.settings.set(CFG.id, 'debug', enabled === true)
	}
});
