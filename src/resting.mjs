import { CFG, getSettings } from './common.mjs';
import './settings.mjs';

const fu = foundry.utils;

/* global RollPF */

async function printChatCard(actor, healing, data) {
	const eventString = healing.longTerm ? 'Koboldworks.Resting.EventLongTerm' : 'Koboldworks.Resting.Event';
	const tdata = {
		healing,
		description: game.i18n.format(eventString, { hours: healing.time }),
		data,
	};

	// TODO: Display inline roll for healing.
	// console.log(data);
	// <a class='inline-roll inline-result' title='{{data.hp.formula}}' data-roll='{{data.hp.json}}'><i class='fas fa-dice-d20'></i> {{data.hp.label}}</a>

	const msgData = {
		// type: CONST.CHAT_MESSAGE_TYPES.OTHER,
		// user: game.user.id,
		speaker: ChatMessage.getSpeaker({ actor, token: actor.token }),
		rollMode: game.settings.get('core', 'rollMode'),
		flags: {},
		content: await renderTemplate(`modules/${CFG.id}/template/chat-card.hbs`, tdata),
	};

	fu.setProperty(msgData.flags, 'core.canPopout', false);
	fu.setProperty(msgData.flags, `${CFG.id}.resting`, true);

	// Make most rolls into public or gm roll type
	if (msgData.rollMode === 'blindroll' || msgData.rollMode === 'selfroll' && !game.user.isGM) msgData.rollMode = 'gmroll';
	if (msgData.rollMode === 'gmroll') msgData.whisper = [...game.users.filter(u => u.isGM || u.id === game.user.id)];
	if (msgData.rollMode === 'selfroll') msgData.whisper = [game.user.id];

	ChatMessage.create(msgData);
}

function doHeal(hData, opts, rollData, label) {
	const _rollData = fu.deepClone(rollData),
		healRoll = RollPF.safeRoll(hData.formula || '0', _rollData),
		baseHeal = healRoll?.total ?? 0;
	_rollData.heal = baseHeal;
	const bonusRoll = RollPF.safeRoll(hData.bonus || '0', _rollData),
		bonus = bonusRoll?.total ?? 0;
	_rollData.bonus = bonus;
	const longTermRoll = opts.longTermCare ? RollPF.safeRoll(hData.longTerm || '0', _rollData) : null,
		longTerm = longTermRoll?.total ?? 0;

	hData.rolls = {
		baseValue: baseHeal,
		bonusValue: bonus,
		longTermValue: longTerm,
		base: healRoll,
		bonus: bonusRoll,
		longTerm: longTermRoll,
		rollData: _rollData,
	};

	if (game.settings.get(CFG.id, 'debug')) {
		console.group(`Heal: ${label}`);
		console.log('@time:', _rollData.time);
		console.log('@fullRest:', _rollData.fullRest);
		console.log('@heal:', baseHeal, hData.formula);
		console.log('@bonusHeal:', bonus, hData.bonus);
		console.log('Long Term:', longTerm, hData.longTerm);
		console.log('Total:', baseHeal + bonus + longTerm);
		console.groupEnd();
	}

	return baseHeal + bonus + longTerm;
}

function effectiveHeal(doneHeal, healthMissing) {
	const remaining = healthMissing - doneHeal;
	return remaining < 0 ? doneHeal + remaining : doneHeal;
}

// Hooks.call('actorRest') triggers only for the user that triggered the rest. No guards are necessary.
/**
 * @param {Actor} actor
 * @param {object} opts Rest options
 * @param {boolean} [opts.restoreHealth]
 * @param {object} updateData
 */
function restEvent(actor, opts, updateData) {
	if (actor == null) return; // Shouldn't happen, but...
	if (!opts.restoreHealth) return;

	// Clear healing applied by the system as it has already been applied
	if (updateData.system?.attributes?.hp)
		delete updateData.system.attributes.hp;
	if (updateData.system?.abilities)
		delete updateData.system.abilities;

	try {
		// Re-implement core behaviour as per https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/issues/607
		const heal = getSettings();

		const hp = actor.system.attributes.hp;

		const rollData = fu.deepClone(actor.getRollData());
		rollData.time = opts.hours;
		const fullRest = rollData.fullRest = RollPF.safeRoll(heal.fullRest, rollData).total ?? 0;

		const hpHeal = doHeal(heal.hp, opts, rollData, 'Normal'),
			nlHeal = doHeal(heal.nl, opts, rollData, 'Nonlethal'),
			ablHeal = doHeal(heal.abl, opts, rollData, 'Ability');

		let totalHealing = 0;

		const healing = { time: opts.hours, fullRest };

		const healData = { system: {} };
		if (hp.value < hp.max) {
			const healedValue = Math.min(hp.value + hpHeal, hp.max);
			const healedAmount = effectiveHeal(hpHeal, hp.max - hp.value);
			healing['hp'] = {
				healed: healedAmount,
				before: hp.value,
				after: healedValue,
				data: heal.hp,
				max: hp.max,
			};
			totalHealing += healedAmount;
			fu.setProperty(healData.system, 'attributes.hp.value', healedValue);
		}
		if (hp.nonlethal > 0) {
			const healedValue = Math.max(0, (hp.nonlethal || 0) - nlHeal);
			const healedAmount = effectiveHeal(nlHeal, hp.nonlethal);
			healing['nl'] = {
				healed: healedAmount,
				before: hp.nonlethal,
				after: healedValue,
				data: heal.nl,
			};
			totalHealing += healedAmount;
			fu.setProperty(healData.system, 'attributes.hp.nonlethal', healedValue);
		}

		const ablTemp = {};
		for (const [key, abl] of Object.entries(actor.system.abilities)) {
			if (abl.damage > 0) {
				const healedValue = Math.max(0, (abl.damage || 0) - ablHeal);
				const healedAmount = effectiveHeal(ablHeal, abl.damage || 0);
				ablTemp[key] = {
					healed: healedAmount,
					before: abl.damage,
					after: healedValue,
					data: heal.abl,
					name: CONFIG.PF1.abilities[key],
				};
				totalHealing += healedAmount;
				fu.setProperty(healData.system, `abilities.${key}.damage`, healedValue);
			}
		}
		if (Object.keys(ablTemp).length) healing['abl'] = ablTemp;
		healing.longTerm = opts.longTermCare;
		healing.didHeal = totalHealing > 0;
		healing.notifyUseRestoration = heal.declareUseRestore && opts.restoreDailyUses;

		// ^ End re-implementation

		// mergeObject and setProperty ensure the data is not in flat format
		fu.mergeObject(updateData, healData);

		// Tell system to not do healing
		// opts.restoreHealth = false;// The system has already done this, setting it to false does nothing

		console.log(`%cRESTING%c 🛏️ | %c${actor.name}%c rested. Healing:`,
			CFG.COLORS.main, CFG.COLORS.unset, CFG.COLORS.label, CFG.COLORS.unset,
			{ healing: fu.deepClone(healing), healData: fu.deepClone(healData), updateData: fu.deepClone(updateData) });

		if (heal.chatCard) printChatCard(actor, healing, heal);
	}
	catch (err) {
		console.error('RESTING | Error:', err);
	}
}

Hooks.once('init', () => {
	console.log('%cRESTING%c 🛏️ | Settings: ',
		CFG.COLORS.main, CFG.COLORS.unset,
		getSettings());

	console.log(`%cRESTING%c 🛏️ | %c${game.modules.get(CFG.id).version}%c | READY`,
		CFG.COLORS.main, CFG.COLORS.unset, CFG.COLORS.number, CFG.COLORS.unset);
});

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
function reformatRestingCard(cm, [html]) {
	if (!cm.getFlag(CFG.id, 'resting')) return;

	html.classList.add('koboldworks', 'resting');

	const cfg = getSettings();
	const r = html.querySelector('.resting-result');

	const actor = ChatMessage.getSpeakerActor(cm.speaker);
	if (!cfg.transparency && actor) {
		if (actor.permission < CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER)
			r.querySelector('.healing').remove();
	}

	if (cfg.deltaOnly)
		r.querySelectorAll('.value.original,span.final,span.max')
			.forEach(el => el.remove());
}

Hooks.on('renderChatMessage', reformatRestingCard);
Hooks.on('pf1PreActorRest', restEvent);
