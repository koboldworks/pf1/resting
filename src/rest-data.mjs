/**
 * Basic healing formula inform
 */
export class HealFormula extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			// Base formula
			formula: new fields.StringField(),
			// Bonus formula
			bonus: new fields.StringField(),
			// Long term care modifier
			longTerm: new fields.StringField(),
		};
	}
}

export class RestData extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			fullRest: new fields.StringField({ initial: 'max(1, floor(@time / 24) * 2)' }),
			hp: new fields.EmbeddedDataField(HealFormula, {
				initial: ({
					formula: '@attributes.hd.total[hd] * @fullRest[fullRest]',
					bonus: '',
					longTerm: '@heal[heal]'
				})
			}),
			// unaffected by long term care
			nl: new fields.EmbeddedDataField(HealFormula, {
				initial: ({
					formula: '@attributes.hd.total[hd] * @time[time]',
					bonus: '',
					longTerm: '@heal[heal]'
				})
			}),
			// per ability score
			abl: new fields.EmbeddedDataField(HealFormula, {
				initial: ({
					formula: '1 * @fullRest[fullRest]',
					bonus: '',
					longTerm: '@heal[heal]'
				})
			}),
		};
	}
}
