export const CFG = /** @type {const} */ ({
	id: 'koboldworks-pf1-resting',
	COLORS: {
		main: 'color:mediumseagreen',
		label: 'color:darkseagreen',
		number: 'color:mediumpurple',
		unset: 'color:unset',
	},
});

Object.freeze(CFG);

export const getSettings = () => {
	const cfg = game.settings.get(CFG.id, 'resting');
	['chatCard', 'transparency', 'deltaOnly', 'declareUseRestore']
		.forEach(key => cfg[key] = game.settings.get(CFG.id, key));
	return cfg;
};
