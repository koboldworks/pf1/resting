# Resting override for Pathfinder 1e

Allows overriding PF1 healing logic. The core PF1 system does not support granular overriding, so this re-implements much of the functionality.

You have control over hit point, nonlethal damage, and ability score damage healing.

The formulas are automatically tested to be valid by creating a temporary actor which _may_ cause a small hitch when it is modified.

Automatic on rest button usage once configured.

By default this adds 24 hour rest support which is not present in PF1 (v0.77.23 or older).

## Details

### Rationale

Homebrew support and resting chat cards.

### Configuration

Module settings includes formula dialog where appropriate formula can be entered.

![Rest Configuration](./img/screencaps/rest-config.png)

The formula has access to the resting actor's roll data.

## Limitations

None known.

## Compatibility

None known.

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/resting/-/releases/permalink/latest/downloads/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
