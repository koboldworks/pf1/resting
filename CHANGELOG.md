# Change Log

## 2.2.1.1

- Fix: Strange behaviour in resting with arcanist.  
  This may require fix on PF1 side as well to be properly fixed.

## 2.2.1

- Removed: Setting migration from old versions as it was buggy and disabled all settings for new worlds.
- Change: Module enabled toggle removed. Disable the module from now on if toggling is desired.
- Change: Minimum compatible PF1 version increased to 9.4

## 2.2.0

- Change: PF1v9 compatibility. Minimum required version increased to v9.

## 2.1.0.1

- Fix: Chat card toggle was nonfunctional.
- Fix: Chat card labels were missing.

## 2.1.0

- Fix: PF1 API had breaking change: pf1ActorRest is post rest now, pf1PreActorRest is what should be used.
- Fix: Configuration dialog closed on enter.
- Change: Formula configuration displays basic errors in validating the formula.
- Change: Most settings moved out of the configuration dialog. Migration is run by GM as necessary.

## 2.0.0

- Fixed: Illegible private resting card.
- Foundry v10 support, PF1 0.82.x support. Support for older versions dropped.

## 1.0.1

- Changed: Default to enabled with all settings, including enabling the module itself by default.
- Added: Transparency option.
- Added: Delta only option, display only healing done, not old and new values.
- Changed: Full transparency now shows max HP.
- Removed: Migration from the previous monolithic combination module.

## 1.0.0.2

- Fixed: Required Field "name" not present in ActorData

## 1.0.0.1

- Fixed: Rollmode was not respected. Now non-public rollmodes are all treated as gmroll.

## 1.0.0 Dedicated module

- Changed: Use RollPF.safeRoll() instead of Foundry's Roll() to provide more consistent roll behaviour with rest of PF1.
